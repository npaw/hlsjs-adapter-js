/* global Hls */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Hlsjs = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = null
    if (this.tag) {
      ret = this.tag.currentTime
    }
    return ret
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    var ret = null
    if (this.tag) {
      ret = this.tag.playbackRate
    }
    return ret
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    var ret = null
    if (this.tag) {
      ret = this.tag.webkitDroppedFrameCount
    }
    return ret
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = null
    if (this.tag && !this.getIsLive()) {
      ret = this.tag.duration
    }
    return ret
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var ret = null
    if (this.player.currentLevel !== -1 && this.player.levels[this.player.currentLevel]) {
      ret = this.player.levels[this.player.currentLevel].bitrate
    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    var ret = null
    if (this.player.currentLevel !== -1) {
      var level = this.player.levels[this.player.currentLevel]
      if (level) {
        if (level.name) {
          ret = level.name
        } else {
          if (level.bitrate) {
            ret = youbora.Util.buildRenditionString(level.width, level.height, level.bitrate)
          }
        }
      }
    }
    return ret
  },

  /** Override to return true if live and false if VOD */
  getIsLive: function () {
    var ret = null
    if (this.player.levels && this.player.levels[this.player.currentLevel]) {
      ret = this.player.levels[this.player.currentLevel].details.live
    }
    return ret
  },

  getLatency: function () {
    var latency = this.player.latency
    return latency ? latency * 1000 : undefined
  },

  /** Override to return resource URL. */
  getResource: function () {
    var url = this.player.url

    if (url === null || url === '') {
      url = window.flowplayer.hls.url
    }

    return url
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    if (typeof Hls !== 'undefined') {
      return Hls.version
    }

    if (window.flowplayer !== null) {
      return window.flowplayer.hls.version
    }

    return null
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Hls.js'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    if (!this.mediaFound) {
      this._registerHlsRef()
    } else {
      if (!this.hlsReference && !this.hlsMediaReference) {
        this.tag = this.player ? this.player.media : null
        this._registerHlsRef()
      }
      // References
      this.references = {
        play: this.playListener.bind(this),
        pause: this.pauseListener.bind(this),
        playing: this.playingListener.bind(this),
        seeking: this.seekingListener.bind(this),
        error: this.errorListener.bind(this),
        ended: this.endedListener.bind(this),
        progress: this.progressListener.bind(this),
        waiting: this.bufferingListener.bind(this)
      }

      // Register listeners
      for (var key in this.references) {
        this.tag.addEventListener(key, this.references[key])
      }
    }
  },

  _registerHlsRef: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    // this.monitorPlayhead(true, true)
    this.hlsReference = this.hlsErrorListener.bind(this)
    this.hlsMediaReference = this.getTagListener.bind(this)
    this.player.on('hlsError', this.hlsReference, this)
    this.player.on('hlsMediaAttached', this.hlsMediaReference)
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()
    // unregister listeners
    if (this.player) {
      if (this.hlsReference) {
        this.player.off('hlsError', this.hlsReference, this)
      }
      if (this.hlsMediaReference) {
        this.player.off('hlsMediaAttached', this.hlsMediaReference)
      }
      this.hlsReference = null
      this.hlsMediaReference = null
    }
    if (this.tag && this.references) {
      for (var key in this.references) {
        this.tag.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  progressListener: function (e) {
    if (!this.flags.isStarted) {
      if (this.getPlayhead() > 1 &&
        (this.startedOnce || !this.plugin.getIsLive())) {
        this.fireStart({}, 'progressListener')
        this.fireJoin({}, 'progressListener')
      }
    } else if (!this.flags.isJoined && this.plugin.getIsLive()) {
      this.fireJoin({}, 'progressListener')
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireStart({}, 'playListener')
    this.startedOnce = true
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    if (!this.flags.isBuffering) {
      this.firePause({}, 'pauseListener')
      this.pauseStamp = e.timeStamp
    }
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    /*if (this.flags.isPaused && this.chronos.pause.getDeltaTime() <= 100 && this.pauseStamp !== e.timeStamp) {
      this.fireSeekBegin({},false)
      this.fireSeekEnd()
    }*/
    this.fireJoin({}, 'playingListener')
    this.fireBufferEnd({}, 'playingListener')
    this.fireSeekEnd({}, 'playingListener')
    this.fireResume({}, 'playingListener')
  },

  seekingListener: function (e) {
    this.fireSeekBegin({}, false, 'seekingListener')
  },

  bufferingListener: function(e) {
    if (!this.tag || this.tag.readyState === 2) {
      this.fireBufferBegin({}, false, 'bufferingListener')
    }
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    if (e && e.data && e.data.code && e.data.message) {
      this.fireError(e.data.code, e.data.message, undefined, undefined, 'errorListener')
    } else if (e && e.data && e.data.code) {
      this.fireError(e.data.code, 'PLAY FAILURE', undefined, undefined, 'errorListener')
    } else if (this.tag && this.tag.error && this.tag.error.code) {
      this.fireError(this.tag.error.code, 'PLAY FAILURE', undefined, undefined, 'errorListener')
    }
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop({}, 'endedListener')
  },

  /** Listener for 'error' hls event. */
  hlsErrorListener: function (event, error) {
    if (error.fatal) {
      this.fireFatalError(error.details, 'PLAYER FAILURE', undefined, undefined, 'hlsErrorListener')
    } //Nonfatal are ignored
  },

  getTagListener: function () {
    this.tag = this.player ? this.player.media : null
    this.mediaFound = true
    this.registerListeners()
  }
})

module.exports = youbora.adapters.Hlsjs
