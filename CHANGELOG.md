## [6.8.11] - 2024-10-10
### Library
- Packaged with `lib 6.8.59`

## [6.8.10] - 2024-07-10
### Fixed
- Change `let` statement to `var` to fix error with Hisense Smart TVs.
### Library
- Packaged with `lib 6.8.57`

## [6.8.9] - 2024-05-23
### Added
- Fallback to flowplayer's `getResource` and `getPlayerVersion` methods.
### Library
- Packaged with `lib 6.8.56`

## [6.8.8] - 2023-03-17
### Added
- On errorListener, fire the error using error object if we can get error code and/or message

## [6.8.7] - 2022-10-28
### Library
- Packaged with `lib 6.8.34`

## [6.8.6] - 2022-10-26
### Added
- Report triggered events parameter for the fired plugin events
### Library
- Packaged with `lib 6.8.33`

## [6.8.5] - 2022-06-28
### Added
- Additional check to prevent reporting wrong buffer events
### Library
- Packaged with `lib 6.8.24`

## [6.8.4] - 2022-05-26
### Library
- Packaged with `lib 6.8.21`

## [6.8.3] - 2022-03-29
### Added
-  `getLatency` implementation to track latency for live videos
### Library
- Packaged with `lib 6.8.15`

## [6.8.2] - 2022-01-03
### Added
- Reduced seek and buffer logic to the minimum: first event defines what is sent after: seeking is seek, waiting is buffer, pause is pause, playing ends all the previous. No playhead monitor

## [6.8.1] - 2021-12-15
### Added
- Corrected the behaviour of buffers and seeks when both events are being reported mixed
### Library
- Packaged with `lib 6.8.10`

## [6.8.0] - 2021-11-25
### Added
- Improved control of buffer and seek events using video element api
### Library
- Packaged with `lib 6.8.9`

## [6.7.6] - 2021-07-23
### Added
- Increased margin of playhead value to detect jointime with timeupdate event to 1 second instead of 0.2
### Library
- Packaged with `lib 6.7.41`

## [6.7.5] - 2021-07-14
### Added
- Check to prevent fake resumes and buffers while playing ads
### Library
- Packaged with `lib 6.7.40`

## [6.7.4] - 2020-09-02
### Library
- Packaged with `lib 6.7.16`

## [6.7.3] - 2020-07-21
### Library
- Packaged with `lib 6.7.12`

## [6.7.2] - 2020-06-08
### Fixed
- Removed non fatal errors reported when buffering
### Library
- Packaged with `lib 6.7.7`

## [6.7.1] - 2020-05-21
### Fixed
- Event mixing between buffer and seek

## [6.7.0] - 2020-05-06
### Fixed
- Some errors not being tracked
- Jointime for some live content
### Added
- Code refactor
### Library
- Packaged with `lib 6.7.6`

## [6.5.4] - 2019-10-16
### Fixed
- Autoplay non blocked detection for VoD

## [6.5.3] - 2019-10-15
### Library
- Packaged with `lib 6.5.17`

## [6.5.2] - 2019-08-30
### Library
- Packaged with `lib 6.5.14`

## [6.5.1] - 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.5.0] - 2019-05-31
### Library
- Packaged with `lib 6.5.1`

## [6.4.7] - 2019-04-29
### Fixed
- Fake start prevented when autoplay is enabled and blocked by the browser again, checking both `progress` and `play` events
### Library
- Packaged with `lib 6.4.25`

## [6.4.6] - 2019-04-05
### Library
- Packaged with `lib 6.4.23`

## [6.4.5] - 2019-01-03
### Library
- Packaged with `lib 6.4.13`

## [6.4.4] - 2018-11-23
### Fixed
- Issues related to register and unregister listeners

## [6.4.3] - 2018-11-12
### Fixed
- Fake start prevented when autoplay is enabled and blocked by the browser
### Library
- Packaged with `lib 6.4.9`

## [6.4.2] - 2018-09-26
### Fixed
- Workaround for Hls not available

## [6.4.1] - 2018-08-30
### Library
- Packaged with `lib 6.4.5`

## [6.4.0] - 2018-08-29
### Library
- Packaged with `lib 6.4.3`

## [6.3.0] - 2018-07-09
### Library
- Packaged with `lib 6.3.2`

## [6.2.2] - 2018-05-16
### Library
- Packaged with `lib 6.2.6`
### Fixed
- Workaround for Hls not available

## [6.2.1] - 2018-04-26
### Library
- Packaged with `lib 6.2.2`
### Fixed
- Typo unsuscribing listeners

## [6.2.0] - 2018-04-09
### Library
- Packaged with `lib 6.2.0`

## [6.1.0] - 2018-02-16
### Library
- Packaged with `lib 6.1.12`
